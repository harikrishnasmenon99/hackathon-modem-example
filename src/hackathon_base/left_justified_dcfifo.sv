
module left_justified_dcfifo#(parameter W = 16)(
  input  wire         aclr,
  input  wire [W-1:0] data,
  input  wire         rdclk,
  input  wire         rdreq,
  input  wire         wrclk,
  input  wire         wrreq,
  output wire [W-1:0] q,
  output wire         rdempty,
  output wire         wrfull
);
  dcfifo dcfifo_component (
    .aclr      (aclr),
    .data      (data),
    .rdclk     (rdclk),
    .rdreq     (rdreq),
    .wrclk     (wrclk),
    .wrreq     (wrreq),
    .q         (q),
    .rdempty   (rdempty),
    .wrfull    (wrfull),
    .eccstatus (),
    .rdfull    (),
    .rdusedw   (),
    .wrempty   (),
    .wrusedw   ());
  defparam
    dcfifo_component.intended_device_family = "Cyclone V",
    dcfifo_component.lpm_numwords = 8,
    dcfifo_component.lpm_showahead = "OFF",
    dcfifo_component.lpm_type = "dcfifo",
    dcfifo_component.lpm_width = W,
    dcfifo_component.lpm_widthu = 3,
    dcfifo_component.overflow_checking = "ON",
    dcfifo_component.rdsync_delaypipe = 4,
    dcfifo_component.read_aclr_synch = "OFF",
    dcfifo_component.underflow_checking = "ON",
    dcfifo_component.use_eab = "ON",
    dcfifo_component.write_aclr_synch = "OFF",
    dcfifo_component.wrsync_delaypipe = 4;

endmodule
