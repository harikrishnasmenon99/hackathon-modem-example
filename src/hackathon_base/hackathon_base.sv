`default_nettype none

module hackathon_base (
  input  wire        clk,
  output wire        reset_o,

  input  wire [15:0] analog_data_i,
  output wire        analog_ready_i,

  output wire [15:0] analog_data_o,
  output wire        analog_valid_o,

  input  wire  [7:0] digital_data_i,
  input  wire        digital_valid_i,
  output wire        digital_ready_i,

  output wire  [7:0] digital_data_o,
  output wire        digital_valid_o,
  input  wire        digital_ready_o,

  input  wire        uart_rx,
  output wire        uart_tx,

  output wire        i2c_scl,
  inout  wire        i2c_sda,

  input  wire        aud_adcdat,
  output wire        aud_adclrck,
  output wire        aud_bclk,
  output wire        aud_dacdat,
  output wire        aud_daclrck,
  output wire        aud_xck
);

  logic [7:0] reset_timer = '0;
  logic       reset = 1'd1;
  always @(posedge clk) begin
    if (reset_timer == '1) begin
      reset <= 1'd0;
    end else begin
      reset_timer <= reset_timer + 1'd1;
      reset <= 1'd1;
    end
  end

  logic [7:0] rx_fifo_data;
  logic       rx_fifo_rdreq;
  logic       rx_fifo_wrreq;
  logic       rx_fifo_empty;
  logic [7:0] rx_fifo_q;
  logic       rx_fifo_full;

  logic [7:0] tx_fifo_data;
  logic       tx_fifo_rdreq;
  logic       tx_fifo_wrreq;
  logic       tx_fifo_empty;
  logic [7:0] tx_fifo_q;
  logic       tx_fifo_full;

  StreamBus #(.N(8)) bus_rx (.clk(clk), .rst(~reset));
  StreamBus #(.N(8)) bus_tx (.clk(clk), .rst(~reset));

  assign rx_fifo_data = bus_rx.data;
  assign rx_fifo_wrreq = bus_rx.valid;
  assign bus_rx.ready = !rx_fifo_full;
  assign rx_fifo_rdreq = digital_ready_o;
  assign digital_valid_o = !rx_fifo_empty;
  assign digital_data_o = rx_fifo_q;

  assign tx_fifo_data = digital_data_i;
  assign tx_fifo_wrreq = digital_valid_i;
  assign digital_ready_i = !tx_fifo_full;
  assign tx_fifo_rdreq = bus_tx.ready;
  assign bus_tx.valid = !tx_fifo_empty;
  assign bus_tx.data = tx_fifo_q;

  uart_rx #(.F(50000000), .BAUD(115200)) urx (
    .rx(uart_rx),
    .bus(bus_rx));

  uart_tx #(.F(50000000), .BAUD(115200)) utx (
    .bus(bus_tx),
    .tx(uart_tx));

  scfifo rx_fifo (
    .aclr         (),
    .clock        (clk),
    .data         (rx_fifo_data),
    .rdreq        (rx_fifo_rdreq),
    .wrreq        (rx_fifo_wrreq),
    .empty        (rx_fifo_empty),
    .q            (rx_fifo_q),
    .usedw        (),
    .almost_empty (),
    .almost_full  (),
    .eccstatus    (),
    .full         (rx_fifo_full),
    .sclr         (reset));
  defparam
    rx_fifo.add_ram_output_register  = "ON",
    rx_fifo.enable_ecc  = "FALSE",
    rx_fifo.intended_device_family  = "Stratix 10",
    rx_fifo.lpm_numwords  = 256,
    rx_fifo.lpm_showahead  = "ON",
    rx_fifo.lpm_type  = "scfifo",
    rx_fifo.lpm_width  = 8,
    rx_fifo.lpm_widthu  = 8,
    rx_fifo.overflow_checking  = "ON",
    rx_fifo.underflow_checking  = "ON",
    rx_fifo.use_eab  = "ON";

  scfifo tx_fifo (
    .aclr         (),
    .clock        (clk),
    .data         (tx_fifo_data),
    .rdreq        (tx_fifo_rdreq),
    .wrreq        (tx_fifo_wrreq),
    .empty        (tx_fifo_empty),
    .q            (tx_fifo_q),
    .usedw        (),
    .almost_empty (),
    .almost_full  (),
    .eccstatus    (),
    .full         (tx_fifo_full),
    .sclr         (reset));
  defparam
    tx_fifo.add_ram_output_register  = "ON",
    tx_fifo.enable_ecc  = "FALSE",
    tx_fifo.intended_device_family  = "Stratix 10",
    tx_fifo.lpm_numwords  = 32,
    tx_fifo.lpm_showahead  = "ON",
    tx_fifo.lpm_type  = "scfifo",
    tx_fifo.lpm_width  = 8,
    tx_fifo.lpm_widthu  = 5,
    tx_fifo.overflow_checking  = "ON",
    tx_fifo.underflow_checking  = "ON",
    tx_fifo.use_eab  = "ON";

analog_ss analog_ss(
  .clk        (clk),
  .reset      (reset),
  .reset_o 	  (reset_o),
  .address    (2'b0),
  .byteenable (4'b0),
  .read       (1'b0),
  .write      (1'b0),
  .writedata  (32'b0),
  .readdata   (),
  .waitrequest(),
  .TX0_data   (analog_data_i),
  .TX0_valid  (1'b1),
  .TX0_ready  (analog_ready_i),
  .TX1_data   (),
  .TX1_valid  (),
  .TX1_ready  (),
  .RX0_data   (analog_data_o),
  .RX0_valid  (analog_valid_o),
  .RX0_ready  (1'b1),
  .RX1_data   (),
  .RX1_valid  (),
  .RX1_ready  (),
  .ADCDAT     (aud_adcdat),
  .ADCLRCK    (aud_adclrck),
  .BCLK       (aud_bclk),
  .XCLK       (aud_xck),
  .DACDAT     (aud_dacdat),
  .DACLRCK    (aud_daclrck),
  .I2C_SCL    (i2c_scl),
  .I2C_SDA    (i2c_sda)
);

endmodule

`default_nettype wire
