
`default_nettype none

module analog_ss(

  input  wire clk,
  input  wire reset,
  output wire reset_o,
  
  input wire [1:0] address,
  input wire [3:0] byteenable,
  input wire read,
  input wire write,
  input wire [31:0] writedata,
  output wire [31:0] readdata,
  output wire waitrequest,
  
  input wire [15:0] TX0_data,
  input wire TX0_valid,
  output wire TX0_ready,
  
  input wire [15:0] TX1_data,
  input wire TX1_valid,
  output wire TX1_ready,
  
  output wire [15:0] RX0_data,
  output wire RX0_valid,
  input wire RX0_ready,
  
  output wire [15:0] RX1_data,
  output wire RX1_valid,
  input wire RX1_ready,
  
  input wire ADCDAT,
  output wire ADCLRCK,
  output wire BCLK,
  output wire XCLK,
  output wire DACDAT,
  output wire DACLRCK,
  
  output wire I2C_SCL,
  inout wire I2C_SDA

);

logic reset_bclk;

assign reset_o = reset || reset_bclk;

codec_config_audio_and_video_config_0 codec_config_inst (
  .clk         (clk),
  .reset       (reset || reset_bclk),
  .address     (address),
  .byteenable  (byteenable),
  .read        (read),
  .write       (write),
  .writedata   (writedata),
  .readdata    (readdata),
  .waitrequest (waitrequest),
  .I2C_SDAT    (I2C_SDA),
  .I2C_SCLK    (I2C_SCL)
);

codec_bclk_audio_pll_0 cclk_pll(
  .ref_clk_clk        (clk),
  .ref_reset_reset    (reset),
  .audio_clk_clk      (XCLK),
  .reset_source_reset (reset_bclk)
);

left_justified#(.DWIDTH(16)) left_justified_inst(
  .clk       (clk),
  .reset     (reset || reset_bclk),
  .tx0_data  (TX0_data),
  .tx0_valid (TX0_valid),
  .tx0_ready (TX0_ready),
  .tx1_data  (TX1_data),
  .tx1_valid (TX1_valid),
  .tx1_ready (TX1_ready),
  .rx0_data  (RX0_data),
  .rx0_valid (RX0_valid),
  .rx0_ready (RX0_ready),
  .rx1_data  (RX1_data),
  .rx1_valid (RX1_valid),
  .rx1_ready (RX1_ready),
  .mclk      (XCLK),
  .bclk      (BCLK),
  .adcdat    (ADCDAT),
  .adclrck   (ADCLRCK),
  .dacdat    (DACDAT),
  .daclrck   (DACLRCK)
);
 
endmodule

`default_nettype wire
