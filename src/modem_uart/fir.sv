
module fir#(parameter N = 8)(
input logic clk,
input logic reset,
input logic signed [15:0] data_i,
input logic valid_i,
output logic  ready_i,
output logic signed [15:0] data_o,
output logic  valid_o,
input logic ready_o
);

logic signed [15:0] coef [N:0];
initial begin
  coef[0]  = 2**14 *  0.0126;
  coef[1]  = 2**14 *  0.0419;
  coef[2]  = 2**14 *  0.1212;
  coef[3]  = 2**14 *  0.2107;
  coef[4]  = 2**14 *  0.2500;
  coef[5]  = 2**14 *  0.2107;
  coef[6]  = 2**14 *  0.1212;
  coef[7]  = 2**14 *  0.0419;
  coef[8]  = 2**14 *  0.0126;
end
 
logic signed [N:0][15:0] z;
logic signed [N:0][15:0] mul;
logic signed      [15:0] sum;
logic [N:0] valid_d;

always_comb begin
  sum = 0;
  for(int i=0;i<N+1;i++)
    sum = sum + data_pkg::multiply_Q(z[i],coef[i]);
end

always_ff@(posedge clk) begin
if(ready_o & valid_i) begin
  z <= {z[N-1:0],data_i};
  valid_d <= {valid_d[N-1:0],1'b1};
  data_o <= sum;
end
if(reset)
  valid_d <= 0;
end

assign ready_i = ready_o;
assign valid_o = valid_d[N] & valid_i;

endmodule
